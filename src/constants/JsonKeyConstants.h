namespace JsonKeys
{
    static const char *type = "type";
    static const char *id = "id";
    static const char *requestId = "requestId";
    static const char *payload = "payload";
    static const char *execution = "execution";
    static const char *states = "states";
    static const char *status = "status";
    static const char *errorCode = "errorCode";
    static const char *on = "on";
    static const char *color = "color";
    static const char *command = "command";
    static const char *commands = "commands";
    static const char *params = "params";
}
