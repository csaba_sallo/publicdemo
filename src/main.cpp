#include <Arduino.h>
#include "models/PayLoadModel.h"
#include "enums/CommandEnums.h"
#include <iostream>
#include <sstream>
#include "models/StateModel.h"
#include "models/CommandParamModel.h"
#include "models/BaseModel.h"
//#include "models/JSONBase.h"
#include "models/RequestModel.h"
#include "models/CommandModel.h"

const char *TAG = "Main.cpp";

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  CommandModel *lCommandModel = new CommandModel(5);
  lCommandModel->setStatus(CommandStatusEnum::Success);
  lCommandModel->setErrorCode(CommandErrorCodeEnum::SameColor);

  CommandModel *lCommandModel2 = new CommandModel(10);
  lCommandModel2->setStatus(CommandStatusEnum::Error);
  lCommandModel2->setErrorCode(CommandErrorCodeEnum::SameColor);

  PayLoadModel *lPayl = new PayLoadModel();
  lPayl->setRequest(new RequestModel(RequestTypeEnum::Execute, "reqIdyo"));
  lPayl->setState(new StateModel(true, "pinkipink"));
  lPayl->addCommand(lCommandModel);
  lPayl->addCommand(lCommandModel2);

  std::string jsonSer = lPayl->Serializing();
  Logger::verbose(TAG, "Serialize json: ", jsonSer);

  std::string jsonDes2 = "{\"type\":\"EXECUTE\",\"requestId\":\"reqId2\", \"execution\" : [{\"id\":1,\"command\":\"OnOff\",\"params\":{ \"on\":true }}, {\"id\":2,\"command\":\"SetColor\",\"params\":{ \"color\":\"#akarmiColor\" }}]}";
  PayLoadModel *lPay2 = new PayLoadModel();
  if (lPay2->Deserializing(&jsonDes2))
  {
    Logger::verbose(TAG, "lpay2: ", lPay2->toString());
  }

  Logger::verbose(TAG, "Memory heap: ", ESP.getFreeHeap());

  delete lPayl;
  delete lPay2;

  std::cout << "----------" << std::endl;
}