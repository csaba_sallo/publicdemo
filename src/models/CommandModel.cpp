#include "CommandModel.h"

/*-----------------Constructors------------*/

CommandModel::CommandModel()
{
    Logger::info(this->TAG(), "empty constructor");
}

CommandModel::CommandModel(const CommandModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

CommandModel::CommandModel(const USHORT pID)
{
    Logger::info(this->TAG(), "constructor with ID");
    this->ID = pID;
}

/*-----------------Destructors------------*/

CommandModel::~CommandModel()
{
    Logger::info(this->TAG(), "destructor");
    delete this->param;
    this->param = NULL;
}

/*-----------------Gets/Sets--------------*/

USHORT CommandModel::getID() const
{
    return this->ID;
}

void CommandModel::setID(const USHORT pID)
{
    this->ID = pID;
}

CommandEnum CommandModel::getCommand() const
{
    return this->command;
}

std::string CommandModel::getCommandString() const
{
    try
    {
        return EnumMapping::getNameForValue(MappedCommand, this->command);
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
    return NULL;
}

void CommandModel::setCommand(const CommandEnum pCommand)
{
    this->command = pCommand;
}

void CommandModel::setCommandByString(const std::string pCommand)
{
    try
    {
        this->setCommand(EnumMapping::getValueForName(MappedCommand, pCommand));
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
}

CommandStatusEnum CommandModel::getStatus() const
{
    return this->status;
}

std::string CommandModel::getStatusString() const
{
    try
    {
        return EnumMapping::getNameForValue(MappedCommandStatus, this->status);
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
    return NULL;
}

void CommandModel::setStatus(const CommandStatusEnum pStatus)
{
    this->status = pStatus;
}

void CommandModel::setStatusByString(const std::string pStatus)
{
    try
    {
        this->setStatus(EnumMapping::getValueForName(MappedCommandStatus, pStatus));
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
}

CommandParamModel *CommandModel::getParam() const
{
    return this->param;
}

void CommandModel::setParam(CommandParamModel *pParam)
{
    this->param = pParam;
}

CommandErrorCodeEnum CommandModel::getErrorCode() const
{
    return errorCode;
}

std::string CommandModel::getErrorCodeString() const
{
    try
    {
        return EnumMapping::getNameForValue(MappedCommandErrorCode, this->errorCode);
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
    return NULL;
}

void CommandModel::setErrorCode(const CommandErrorCodeEnum pErrorCode)
{
    this->errorCode = pErrorCode;
}

void CommandModel::setErrorCodeByString(const std::string pErrorCode)
{
    try
    {
        this->setErrorCode(EnumMapping::getValueForName(MappedCommandErrorCode, pErrorCode));
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
}

/*-----------------json's things functions-----------*/

bool CommandModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
    Logger::info(this->TAG(), "Deserialize");
    setID(pDoc[JsonKeys::id]);
    setCommandByString(pDoc[JsonKeys::command]);

    if (this->param == NULL)
        this->param = new CommandParamModel();
    this->param->Deserialize(pDoc[JsonKeys::params]);
    return true;
}

bool CommandModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    pDoc[JsonKeys::id] = getID();

    if (getStatus() != CommandStatusEnum::Null)
        pDoc[JsonKeys::status] = getStatusString();

    if (getErrorCode() != CommandErrorCodeEnum::Null)
        pDoc[JsonKeys::errorCode] = getErrorCodeString();

    return true;
}

/*-----------------Other functions-----------*/

std::string CommandModel::toString() const
{
    std::string lVal;
    if (this->ID > 0)
        lVal.append("id: ").append(std::to_string(this->ID)).append("; ");

    if (command != CommandEnum::Null)
        lVal.append("command: " + this->getCommandString() + "; ");

    if (status != CommandStatusEnum::Null)
        lVal.append("status: " + this->getStatusString() + "; ");

    if (errorCode != CommandErrorCodeEnum::Null)
        lVal.append("errorCode: " + this->getErrorCodeString() + "; ");

    if (param != NULL)
    {
        std::string lParamString = this->getParam()->toString();
        if (!lParamString.empty())
            lVal.append("\n").append(lParamString);
    }
    if (lVal.empty())
        lVal = "CommandModel is empty";
    else
        lVal.insert(0, "CommandModel: ");
    return lVal;
}
