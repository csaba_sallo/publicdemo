#include "PayLoadModel.h"

/*-----------------Constructors------------*/

PayLoadModel::PayLoadModel()
{
    Logger::info(this->TAG(), "empty constructor");
}

PayLoadModel::PayLoadModel(const PayLoadModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

PayLoadModel::PayLoadModel(RequestModel *pRequest) : BaseModel(pRequest)
{
    Logger::info(this->TAG(), "inherited constructor");
}

/*-----------------Destructors------------*/

PayLoadModel::~PayLoadModel()
{
    Logger::info(this->TAG(), "destructor");
    delete this->state;
    this->state = NULL;

    if (getCommandsSize() > 0)
    {
        for (CommandModel *iCommandModel : this->commands)
        {
            delete iCommandModel;
            iCommandModel = NULL;
        }
    }
}

/*-----------------State vector's functions-------*/

void PayLoadModel::setState(StateModel *pState)
{
    this->state = pState;
}

StateModel *PayLoadModel::getState() const
{
    return this->state;
}

/*-----------------Command vector's functions-------*/

void PayLoadModel::addCommand(CommandModel *pCommand)
{
    this->commands.push_back(pCommand);
}

USHORT PayLoadModel::getCommandsSize() const
{
    return this->commands.size();
}

CommandModel *PayLoadModel::getCommandElementByIt(USHORT pIt) const
{
    return this->commands[pIt];
}

/*-----------------json's things functions-----------*/

bool PayLoadModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
    Logger::info(this->TAG(), "Deserialize");

    if (BaseModel::Deserialize(pDoc))
    {
        if (this->request != NULL)
        {
            if (this->getRequest()->getRequestTypeEnum() == RequestTypeEnum::Execute)
            {
                JsonArrayConst lExecuteArray = pDoc[JsonKeys::execution];
                if (lExecuteArray.isNull() && lExecuteArray.size() <= 0)
                    return true;

                for (JsonObjectConst repo : lExecuteArray)
                {
                    CommandModel *lCommandDes = new CommandModel();
                    if (lCommandDes->Deserialize(repo))
                    {
                        this->addCommand(lCommandDes);
                    }
                }
            }
        }
        return true;
    }

    return false;
}

bool PayLoadModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    if (BaseModel::Serialize(pDoc))
    {
        if (this->state != NULL)
            pDoc[JsonKeys::payload][JsonKeys::states] = this->getState()->Serializing();

        if (this->request != NULL)
        {
            if (this->getRequest()->getRequestTypeEnum() == RequestTypeEnum::Execute && this->getCommandsSize() > 0)
            {
                for (int i = 0; i < this->getCommandsSize(); ++i)
                {
                    pDoc[JsonKeys::payload][JsonKeys::commands][i] = this->getCommandElementByIt(i)->Serializing();
                }
            }
        }
        return true;
    }
    return false;
}

/*-----------------Other functions-----------*/

std::string PayLoadModel::toString() const
{
    std::string lVal;

    if (this->request != NULL)
    {
        std::string lReqString = this->getRequest()->toString();
        if (!lReqString.empty())
            lVal.append("\n").append(lReqString);
    }

    if (this->state != NULL)
    {
        std::string lStateString = this->getState()->toString();
        if (!lStateString.empty())
            lVal.append("\n").append(lStateString);
    }

    if (this->getCommandsSize() > 0)
    {
        for (CommandModel *iCommand : this->commands)
        {
            std::string lCommandString = iCommand->toString();
            if (!lCommandString.empty())
                lVal.append("\n").append(lCommandString);
        }
    }

    if (lVal.empty())
        lVal = "PayLoadModel is empty";
    else
        lVal.insert(0, "PayLoadModel: ");

    return lVal;
}