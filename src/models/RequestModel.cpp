#include "RequestModel.h"

/*-----------------Constructors------------*/

RequestModel::RequestModel() : requestType(RequestTypeEnum::Execute)
{
    Logger::info(this->TAG(), "empty constructor");
}

RequestModel::RequestModel(RequestTypeEnum pRequestType, std::string pRequestId)
{
    Logger::info(this->TAG(), "constructor");
    this->requestType = pRequestType;
    this->requestId = pRequestId;
}

RequestModel::RequestModel(const RequestModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

/*-----------------Destructors------------*/

RequestModel::~RequestModel()
{
    Logger::info(this->TAG(), "destructor");
}

/*-----------------Gets/Sets--------------*/

RequestTypeEnum RequestModel::getRequestTypeEnum() const
{
    return this->requestType;
}

std::string RequestModel::getRequestTypeString() const
{
    try
    {
        return EnumMapping::getNameForValue(MappedRequestType, this->requestType);
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
    return NULL;
}

void RequestModel::setRequestTypeByEnum(RequestTypeEnum pRequestType)
{
    this->requestType = pRequestType;
}

void RequestModel::setRequestTypeByString(std::string pRequestType)
{
    try
    {
        this->setRequestTypeByEnum(EnumMapping::getValueForName(MappedRequestType, pRequestType));
    }
    catch (const EnumMapping::UnknownValueException &e)
    {
        Logger::error(this->TAG(), "Something went wrong: ", e.what());
    }
}

std::string RequestModel::getRequestId() const
{
    return this->requestId;
}

void RequestModel::setRequestId(const std::string pRequestId)
{
    this->requestId = pRequestId;
}

/*-----------------json's things functions-----------*/

bool RequestModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
    Logger::info(this->TAG(), "Deserialize");
    setRequestTypeByString((const char *)pDoc[JsonKeys::type]);
    setRequestId((const char *)pDoc[JsonKeys::requestId]);
    return true;
}

bool RequestModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    pDoc[JsonKeys::type] = getRequestTypeString();
    pDoc[JsonKeys::requestId] = getRequestId();

    return true;
}

/*-----------------Other functions-----------*/

std::string RequestModel::toString() const
{
    std::string lVal;

    if (this->requestType != RequestTypeEnum::Null)
        lVal.append("requestType: " + this->getRequestTypeString() + "; ");

    if (!this->requestId.empty())
        lVal.append("requestId: " + this->requestId + "; ");

    if (lVal.empty())
        lVal = "RequestModel is empty";
    else
        lVal.insert(0, "RequestModel: ");

    return lVal;
}