#pragma once

#include "StateModel.h"
#include "CommandModel.h"
#include "BaseModel.h"

class PayLoadModel : public BaseModel
{
private:
    std::string TAG() const { return "PayLoadModel.cpp"; }

    StateModel *state = NULL;
    std::vector<CommandModel *> commands;

public:
    PayLoadModel();
    PayLoadModel(RequestModel *pRequest);
    PayLoadModel(const PayLoadModel &);
    ~PayLoadModel();

    void setState(StateModel *pState);
    StateModel *getState() const;

    void addCommand(CommandModel *pCommand);
    USHORT getCommandsSize() const;
    CommandModel *getCommandElementByIt(USHORT pIt) const;

    bool Serialize(StaticJsonDocument<384> &pDoc) const;
    bool Deserialize(const StaticJsonDocument<384> &pDoc);

    std::string toString() const;
};
