#include "JSONBase.h"

std::string JSONBase::Serializing() const
{
	StaticJsonDocument<384> lDoc;
	std::string lSerializedJson;

	if (Serialize(lDoc))
	{
		unsigned short int lJsonSize = serializeJson(lDoc, lSerializedJson);
		return lJsonSize > 0 ? lSerializedJson : "";
	}
	return "";
}

bool JSONBase::Deserializing(const std::string *pJsonString)
{
	StaticJsonDocument<384> lDoc;

	DeserializationError lDeserializationError = deserializeJson(lDoc, *pJsonString);

	Logger::info( this->TAG(), "Deserializing - ", pJsonString->c_str());

	if (lDeserializationError)
	{
		Logger::error(this->TAG(), "deserializeJson() failed with code ", lDeserializationError.f_str());
		return false;
	}

	Deserialize(lDoc);

	return true;
}