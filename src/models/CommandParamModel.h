#pragma once

#include "JSONBase.h"
#include "iostream"

class CommandParamModel : public JSONBase
{
private:
    std::string TAG() const { return "CommandParamModel.cpp"; }

    bool on = false;
    std::string color = "";

public:
    CommandParamModel();
    CommandParamModel(const CommandParamModel &);
    CommandParamModel(const bool pOnValue, const std::string pColorValue);
    ~CommandParamModel();

    bool getOnValue() const;
    void setOnValue(const bool pOnValue);

    std::string getColorValue() const;
    void setColorValue(const std::string pName);

    bool Serialize(StaticJsonDocument<384> &pDoc) const;
    bool Deserialize(const StaticJsonDocument<384> &pDoc);

    std::string toString() const;
};
