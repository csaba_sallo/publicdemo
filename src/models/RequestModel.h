#pragma once
#include "enums/BaseEnums.h"
#include "ArduinoJson.h"
#include "JSONBase.h"

class RequestModel : public JSONBase
{
private:
    std::string TAG() const { return "RequestModel.cpp"; }

    RequestTypeEnum requestType = RequestTypeEnum::Null;
    std::string requestId = "";

public:
    RequestModel();
    RequestModel(RequestTypeEnum pRequestType, std::string pRequestId);
    RequestModel(const RequestModel &);
    ~RequestModel();

    RequestTypeEnum getRequestTypeEnum() const;
    std::string getRequestTypeString() const;
    void setRequestTypeByEnum(RequestTypeEnum pRequestType);
    void setRequestTypeByString(std::string pRequestType);

    std::string getRequestId() const;
    void setRequestId(const std::string pRequestId);

    bool Serialize(StaticJsonDocument<384> &pDoc) const;
    bool Deserialize(const StaticJsonDocument<384> &pDoc);

    std::string toString() const;
};