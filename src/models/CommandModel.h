#pragma once

#include "CommandParamModel.h"
#include "enums/CommandEnums.h"
#include "JSONBase.h"

typedef unsigned short int USHORT;

class CommandModel : public JSONBase
{
private:
    std::string TAG() const { return "CommandModel.cpp"; }

    USHORT ID = 0;
    CommandEnum command = CommandEnum::Null;
    CommandStatusEnum status = CommandStatusEnum::Null;
    CommandParamModel *param = NULL;
    CommandErrorCodeEnum errorCode = CommandErrorCodeEnum::Null;

public:
    CommandModel();
    CommandModel(const CommandModel &);
    CommandModel(const USHORT pID);
    ~CommandModel();

    USHORT getID() const;
    void setID(const USHORT pID);

    CommandEnum getCommand() const;
    std::string getCommandString() const;
    void setCommand(const CommandEnum pCommand);
    void setCommandByString(const std::string pCommand);

    CommandStatusEnum getStatus() const;
    std::string getStatusString() const;
    void setStatus(const CommandStatusEnum pStatus);
    void setStatusByString(const std::string pStatus);

    CommandParamModel *getParam() const;
    void setParam(CommandParamModel *pParam);

    CommandErrorCodeEnum getErrorCode() const;
    std::string getErrorCodeString() const;
    void setErrorCode(const CommandErrorCodeEnum pErrorCode);
    void setErrorCodeByString(const std::string pErrorCode);

    bool Serialize(StaticJsonDocument<384> &pDoc) const;
    bool Deserialize(const StaticJsonDocument<384> &pDoc);

    std::string toString()const;
};
