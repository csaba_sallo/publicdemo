#pragma once

#include "RequestModel.h"
class BaseModel : public JSONBase
{
    std::string TAG() const { return "BaseModel.cpp"; }

public:
    BaseModel();
    BaseModel(RequestModel *pRequest);
    BaseModel(const BaseModel &);
    virtual ~BaseModel();

    RequestModel *getRequest() const;
    void setRequest(RequestModel *pRequest);

    virtual bool Serialize(StaticJsonDocument<384> &pDoc) const;
    virtual bool Deserialize(const StaticJsonDocument<384> &pDoc);

protected:
    RequestModel *request = NULL;
};
