#include "CommandParamModel.h"

/*-----------------Constructors------------*/

CommandParamModel::CommandParamModel()
{
    Logger::info(this->TAG(), "empty constructor");
}

CommandParamModel::CommandParamModel(const CommandParamModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

CommandParamModel::CommandParamModel(const bool pOnValue, const std::string pColorValue)
{
    Logger::info(this->TAG(), "constructor");
    this->on = pOnValue;
    this->color = pColorValue;
}

/*-----------------Destructors------------*/

CommandParamModel::~CommandParamModel()
{
    Logger::info(this->TAG(), "destructor");
}

/*-----------------Gets/Sets--------------*/

bool CommandParamModel::getOnValue() const
{
    return this->on;
}

void CommandParamModel::setOnValue(const bool pOnValue)
{
    this->on = pOnValue;
}

std::string CommandParamModel::getColorValue() const
{
    return this->color;
}

void CommandParamModel::setColorValue(const std::string pName)
{
    this->color = pName;
}

/*-----------------json's things functions-----------*/

bool CommandParamModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
    Logger::info(this->TAG(), "Deserialize");
    if (!pDoc[JsonKeys::on].isNull())
        setOnValue(pDoc[JsonKeys::on]);

    if (!pDoc[JsonKeys::color].isNull())
        setColorValue(pDoc[JsonKeys::color]);

    return true;
}

bool CommandParamModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    pDoc[JsonKeys::on] = getOnValue();
    pDoc[JsonKeys::color] = getColorValue();

    return true;
}

/*-----------------Other functions-----------*/

std::string CommandParamModel::toString() const
{
    std::string lVal = "CommandParamModel: ";
    lVal.append("on: ").append(std::to_string(this->on)).append("; ");

    if (!this->color.empty())
        lVal.append("color: " + this->color + "; ");

    return lVal;
}