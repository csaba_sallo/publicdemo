#pragma once
#include "ArduinoJson.h"
#include "utils/Logger.h"
#include "constants/JsonKeyConstants.h"

class JSONBase
{
private:
	std::string TAG() const { return "JSONBase.cpp"; }

	virtual bool Serialize(StaticJsonDocument<384> &pDoc) const;
	virtual bool Deserialize(const StaticJsonDocument<384> &pDoc);

public:
	std::string Serializing() const;
	bool Deserializing(const std::string *pJsonString);
};
