#pragma once

#include "JSONBase.h"
#include "iostream"

class StateModel : public JSONBase
{
private:
    std::string TAG() const { return "StateModel.cpp"; }

    bool on;
    std::string color;

public:
    StateModel();
    StateModel(const StateModel &);
    StateModel(const bool pOnValue, const std::string pColorValue);
    ~StateModel();

    bool getOnValue() const;
    void setOnValue(const bool pOnValue);

    std::string getColorValue() const;
    void setColorValue(const std::string pName);

    bool Serialize(StaticJsonDocument<384> &pDoc) const;
    bool Deserialize(const StaticJsonDocument<384> &pDoc);

    std::string toString() const;
};
