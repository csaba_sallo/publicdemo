#include "BaseModel.h"

/*-----------------Constructors------------*/

BaseModel::BaseModel()
{
    Logger::info(this->TAG(), "empty constructor");
}

BaseModel::BaseModel(RequestModel *pRequest)
{
    Logger::info(this->TAG(), "constructor");
    this->request = pRequest;
}

BaseModel::BaseModel(const BaseModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

/*-----------------Destructors------------*/

BaseModel::~BaseModel()
{
    Logger::info(this->TAG(), "destructor");
    delete request;
    request = NULL;
}

/*-----------------Gets/Sets--------------*/

RequestModel *BaseModel::getRequest() const
{
    return this->request;
}

void BaseModel::setRequest(RequestModel *pRequest)
{
    this->request = pRequest;
}

/*-----------------json's things functions-----------*/

bool BaseModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
    Logger::info(this->TAG(), "Deserialize");

    if (this->request == NULL)
        this->request = new RequestModel();

    return this->request->Deserialize(pDoc);
}

bool BaseModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    if (this->request == NULL)
        return false;

    return this->request->Serialize(pDoc);
}
