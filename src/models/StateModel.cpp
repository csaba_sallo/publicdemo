#include "StateModel.h"

/*-----------------Constructors------------*/

StateModel::StateModel()
{
    Logger::info(this->TAG(), "empty constructor");
}

StateModel::StateModel(const StateModel &)
{
    Logger::info(this->TAG(), "copy constructor");
}

StateModel::StateModel(const bool pOnValue, const std::string pColorValue)
{
    Logger::info(this->TAG(), "constructor");

    this->on = pOnValue;
    this->color = pColorValue;
}

/*-----------------Destructors------------*/

StateModel::~StateModel()
{
    Logger::info(this->TAG(), "destructor");
}

/*-----------------Gets/Sets--------------*/

bool StateModel::getOnValue() const
{
    return this->on;
}

void StateModel::setOnValue(const bool pOnValue)
{
    this->on = pOnValue;
}

std::string StateModel::getColorValue() const
{
    return this->color;
}

void StateModel::setColorValue(const std::string pName)
{
    this->color = pName;
}

/*-----------------json's things functions-----------*/

bool StateModel::Deserialize(const StaticJsonDocument<384> &pDoc)
{
   Logger::info(this->TAG(), "Deserialize");
    if (!pDoc[JsonKeys::on].isNull())
        setOnValue(pDoc[JsonKeys::on]);

    if (!pDoc[JsonKeys::color].isNull())
        setColorValue(pDoc[JsonKeys::color]);

    return true;
}

bool StateModel::Serialize(StaticJsonDocument<384> &pDoc) const
{
    Logger::info(this->TAG(), "Serialize");
    pDoc[JsonKeys::on] = getOnValue();
    pDoc[JsonKeys::color] = getColorValue();

    return true;
}

/*-----------------Other functions-----------*/

std::string StateModel::toString() const
{
    std::string lVal = "StateModel: ";
    lVal.append("on: ").append(std::to_string(this->on)).append("; ");

    if (!this->color.empty())
        lVal.append("color: " + this->color + "; ");

    return lVal;
}