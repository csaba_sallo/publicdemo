#pragma once

#include "iostream"

#define LOG_ENABLED
//#define LOG_INFO_ENABLED
#define LOG_VERBOSE_ENABLED
#define LOG_DEBUG_ENABLED
#define LOG_ERROR_ENABLED

namespace Logger
{
    template <typename... Args>
    inline void printLog(const std::string fileName, Args &&...args)
    {
#ifdef LOG_ENABLED
        std::cout << fileName << ": ";
        ((std::cout << std::forward<Args>(args) << " "), ...);
        std::cout << '\n';
#endif
    }

    template <typename... Args>
    inline void info(const std::string fileName, Args &&...args)
    {
#ifdef LOG_INFO_ENABLED
        printLog(fileName, std::forward<Args>(args)...);
#endif
    }

    template <typename... Args>
    inline void verbose(const std::string fileName, Args &&...args)
    {
#ifdef LOG_VERBOSE_ENABLED
        printLog(fileName, std::forward<Args>(args)...);
#endif
    }

    template <typename... Args>
    inline void debug(std::string fileName, Args &&...args)
    {
#ifdef LOG_DEBUG_ENABLED
        printLog(fileName, std::forward<Args>(args)...);
#endif
    }

    template <typename... Args>
    inline void error(std::string fileName, Args &&...args)
    {
#ifdef LOG_ERROR_ENABLED
        printLog(fileName, std::forward<Args>(args)...);
#endif
    }

}