#pragma once

#include <array>
#include <algorithm>
#include <iostream>
#include <exception>
#include <type_traits>

namespace EnumMapping
{
    class UnknownValueException : public std::runtime_error
    {
    public:
        UnknownValueException(const std::string &name) : std::runtime_error("Unknown value: " + name){};
        UnknownValueException(int value) : std::runtime_error("Unknown name for enum value: " + std::to_string(value)){};
    };

    template <class T>
    struct NameValuePair
    {
        using value_type = T;
        const T value;
        const char *const name;
    };

    template <class Mapping, class V>
    std::string getNameForValue(Mapping a, V value)
    {
        auto pos = std::find_if(std::begin(a), std::end(a), [&value](const typename Mapping::value_type &t)
                                { return (t.value == value); });
        if (pos != std::end(a))
        {
            return pos->name;
        }

        throw UnknownValueException(static_cast<int>(value));
    }

    template <class Mapping>
    typename Mapping::value_type::value_type getValueForName(Mapping a, const std::string &name)
    {
        auto pos = std::find_if(std::begin(a), std::end(a), [&name](const typename Mapping::value_type &t)
                                { return (t.name == name); });
        if (pos != std::end(a))
        {
            return pos->value;
        }
        
        throw UnknownValueException(name);
    }

}

template <typename C, C beginVal, C endVal>
class Iterator
{
  typedef typename std::underlying_type<C>::type val_t;
  int val;

public:
  Iterator(const C &f) : val(static_cast<val_t>(f)) {}
  Iterator() : val(static_cast<val_t>(beginVal)) {}
  Iterator operator++()
  {
    ++val;
    return *this;
  }
  C operator*() { return static_cast<C>(val); }
  Iterator begin() { return *this; } //default ctor is good
  Iterator end()
  {
    static const Iterator endIter = ++Iterator(endVal); // cache it
    return endIter;
  }
  bool operator!=(const Iterator &i) { return val != i.val; }
};


