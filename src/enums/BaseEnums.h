#pragma once

#include "utils/EnumMapping.h"

enum class RequestTypeEnum : char
{
    Null,
    Query,
    Execute
};

const std::array<const EnumMapping::NameValuePair<RequestTypeEnum>, 2> MappedRequestType{{{RequestTypeEnum::Query, "QUERY"},
                                                                                          {RequestTypeEnum::Execute, "EXECUTE"}}};