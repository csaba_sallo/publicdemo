#pragma once

#include "utils/EnumMapping.h"

enum class CommandEnum : unsigned char
{
  Null,
  OnOff,
  SetColor
};

enum class CommandStatusEnum : unsigned char
{
  Null,
  Success,
  Error
};

enum class CommandErrorCodeEnum : unsigned char
{
  Null,
  SameColor
};

const std::array<const EnumMapping::NameValuePair<CommandEnum>, 2> MappedCommand{{{CommandEnum::OnOff, "OnOff"},
                                                                                  {CommandEnum::SetColor, "SetColor"}}};

const std::array<const EnumMapping::NameValuePair<CommandStatusEnum>, 2> MappedCommandStatus{{{CommandStatusEnum::Success, "SUCCESS"},
                                                                                              {CommandStatusEnum::Error, "ERROR"}}};

const std::array<const EnumMapping::NameValuePair<CommandErrorCodeEnum>, 1> MappedCommandErrorCode{{CommandErrorCodeEnum::SameColor, "sameColor"}};
